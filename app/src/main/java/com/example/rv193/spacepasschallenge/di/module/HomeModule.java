package com.example.rv193.spacepasschallenge.di.module;

import com.example.rv193.spacepasschallenge.di.ActivityScope;
import com.example.rv193.spacepasschallenge.ui.home.HomeActivity;
import com.example.rv193.spacepasschallenge.ui.home.HomeContract;
import com.example.rv193.spacepasschallenge.ui.home.HomePresenter;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class HomeModule {

    @ContributesAndroidInjector
    @ActivityScope
    abstract HomeActivity homeActivity();

    @Binds
    public abstract HomeContract.Presenter provideHomePresenter(HomePresenter homePresenter);
}
