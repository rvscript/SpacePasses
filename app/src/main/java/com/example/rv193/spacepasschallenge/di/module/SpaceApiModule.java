package com.example.rv193.spacepasschallenge.di.module;

import android.support.annotation.NonNull;

import com.example.rv193.spacepasschallenge.data.space.SpaceApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class SpaceApiModule {

    @Provides
    @Singleton
    SpaceApi provideSpaceApi(@NonNull Retrofit retrofit) {
        return retrofit.create(SpaceApi.class);
    }
}
