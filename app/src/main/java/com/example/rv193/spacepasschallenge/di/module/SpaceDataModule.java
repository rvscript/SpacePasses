package com.example.rv193.spacepasschallenge.di.module;

import android.support.annotation.NonNull;

import com.example.rv193.spacepasschallenge.data.space.SpaceApi;
import com.example.rv193.spacepasschallenge.data.space.SpaceDataSource;
import com.example.rv193.spacepasschallenge.data.space.SpaceRemoteLayer;
import com.example.rv193.spacepasschallenge.data.space.SpaceRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class SpaceDataModule {

    // Does not need to be a singleton as it's not state-dependent
    @Provides
    SpaceDataSource provideSpaceDataSource(@NonNull SpaceApi spaceApi) {
        return new SpaceRemoteLayer(spaceApi);
    }

    // State dependent, so while @singleton is a bit non-performant, it's handy as we will
    // get providing the same instance through double check and memoization
    @Provides
    @Singleton
    SpaceRepository provideSpaceRepository(@NonNull SpaceDataSource spaceDataSource) {
        return new SpaceRepository(spaceDataSource);
    }
}