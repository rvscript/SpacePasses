package com.example.rv193.spacepasschallenge.data.space;

import android.support.annotation.NonNull;

import com.example.rv193.spacepasschallenge.data.SpaceResponse;

import javax.inject.Inject;

import io.reactivex.Observable;

public class SpaceRemoteLayer implements SpaceDataSource {

    @NonNull
    private final SpaceApi mSpaceApi;

    @Inject
    public SpaceRemoteLayer(@NonNull SpaceApi spaceApi) {
        mSpaceApi = spaceApi;
    }

    @NonNull
    @Override
    public Observable<SpaceResponse> getResponses(double lat, double lng) {
        return mSpaceApi.getSpaceStationPasses(lat, lng);
    }
}