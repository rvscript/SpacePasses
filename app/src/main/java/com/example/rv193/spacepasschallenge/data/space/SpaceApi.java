package com.example.rv193.spacepasschallenge.data.space;

import com.example.rv193.spacepasschallenge.data.SpaceResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SpaceApi {

    @GET("iss-pass.json")
    Observable<SpaceResponse> getSpaceStationPasses(@Query("lat") double lat,
                                                    @Query("lon") double lng);
}