 package com.example.rv193.spacepasschallenge.ui.home;

import android.Manifest;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.example.rv193.spacepasschallenge.R;
import com.example.rv193.spacepasschallenge.data.Response;
import com.example.rv193.spacepasschallenge.databinding.ActivityHomeBinding;
import com.example.rv193.spacepasschallenge.ui.base.BaseActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

 public class HomeActivity extends BaseActivity<HomeContract.Presenter> implements HomeContract.View {

     private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 11;

     private ActivityHomeBinding mBinding;
     private FusedLocationProviderClient mFusedLocationClient;
     private SpacePassAdapter mAdapter;

     @Override
     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         mBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);

         // Fused location client
         mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

         mAdapter = new SpacePassAdapter(new ArrayList<>());
         mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
         mBinding.recyclerView.setAdapter(mAdapter);
         getPresenter().start();
     }

     @Override
     public void showLoading(boolean isLoading) {
         mBinding.setIsLoading(isLoading);
     }

     @Override
     public void askForPermissions() {
         if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                 android.Manifest.permission.ACCESS_FINE_LOCATION)
                 != PackageManager.PERMISSION_GRANTED) {
             if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

                 // Show an explanation to the user as to why we need their permission
                 new AlertDialog.Builder(this)
                         .setTitle(R.string.home_activity_permission_title)
                         .setMessage(R.string.home_activity_permission_message)
                         .setPositiveButton(R.string.continue_label, (dialog, which) -> {

                             // Ask for permission
                             ActivityCompat.requestPermissions(this,
                                     new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                     PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
                         })
                         .setNegativeButton(R.string.cancel, null)
                         .show();
             } else {
                 // No explanation needed, just ask the user
                 ActivityCompat.requestPermissions(this,
                         new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                         PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
             }
         } else {
             getLocation();
         }
     }

     @Override
     public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
         super.onRequestPermissionsResult(requestCode, permissions, grantResults);
         if (requestCode == PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION && grantResults.length > 0
                 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
             getPresenter().loadLocation();
         }
     }

     // TODO 1/30/2018 If there's more time use current location rather than last location; ran out of time
     @Override
     public void getLocation() {
         if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
             mFusedLocationClient.getLastLocation()
                     .addOnSuccessListener(this,
                             lastLocation -> getPresenter().loadResponses(lastLocation.getLatitude(), lastLocation.getLongitude()));
         } else {
             showMessage(R.string.error_location_permission);
         }
     }

     @Override
     public void showResponses(@NonNull List<Response> responses) {
         mAdapter.setItems(responses);
     }

     @Override
     public void showResponse(@NonNull Response response) {
         // TODO 1/30/2018 create another activity where they can view more information
     }
 }
