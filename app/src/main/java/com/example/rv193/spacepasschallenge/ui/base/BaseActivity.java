package com.example.rv193.spacepasschallenge.ui.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public abstract class BaseActivity<T extends BaseContract.Presenter>
        extends AppCompatActivity
        implements BaseContract.View, HasSupportFragmentInjector {

    @Inject T mPresenter;
    @Inject
    DispatchingAndroidInjector<Fragment> mInjector;

    public T getPresenter() {
        return mPresenter;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        androidInject();
        super.onCreate(savedInstanceState);
        mPresenter.onViewAttached(this);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onViewDetached();
        super.onDestroy();
    }

    public void androidInject() {
        AndroidInjection.inject(this);
    }

    @Override
    public void showMessage(@NonNull String string) {
        Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(@NonNull int message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void finishView() {
        finish();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return mInjector;
    }
}
