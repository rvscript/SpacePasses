package com.example.rv193.spacepasschallenge.data.space;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.rv193.spacepasschallenge.data.Response;
import com.example.rv193.spacepasschallenge.data.SpaceResponse;

import java.util.List;
import javax.inject.Inject;
import io.reactivex.Observable;

public class SpaceRepository {

    @NonNull private final SpaceDataSource mDataSource;
    @Nullable private SpaceResponse mCache;
    private boolean mCacheIsDirty = false;

    @Inject
    public SpaceRepository(@NonNull SpaceDataSource remoteLayer) {
        mDataSource = remoteLayer;
    }

    // Return the cache if it's available as an observable
    @NonNull
    public Observable<List<Response>> getResponses(double lat, double lng) {
        if (!mCacheIsDirty && mCache != null) {
            return Observable.just(mCache.getResponse());
        }
        return mDataSource.getResponses(lat, lng)
                .doOnNext(spaceResponse -> {
                    mCache = spaceResponse;
                    mCacheIsDirty = false;
                })
                .flatMap(spaceResponse -> Observable.just(spaceResponse.getResponse()));
    }

    // Just setting the flag to true should force the repository to make the call
    void refresh() {
        mCacheIsDirty = true;
    }
}