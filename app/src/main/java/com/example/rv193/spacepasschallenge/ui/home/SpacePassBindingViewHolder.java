package com.example.rv193.spacepasschallenge.ui.home;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import com.example.rv193.spacepasschallenge.data.Response;
import com.example.rv193.spacepasschallenge.databinding.ItemPassRowBinding;

// TODO 1/30/2018 abstract this class out to hold many types
public class SpacePassBindingViewHolder extends RecyclerView.ViewHolder {

    @NonNull private final ItemPassRowBinding mBinding;

    public SpacePassBindingViewHolder(@NonNull ItemPassRowBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    void bind(@NonNull Response response) {
        mBinding.setPass(response);
        mBinding.executePendingBindings();
    }

    @NonNull
    ItemPassRowBinding getBinding() {
        return mBinding;
    }
}