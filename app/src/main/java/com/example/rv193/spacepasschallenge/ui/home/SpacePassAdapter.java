package com.example.rv193.spacepasschallenge.ui.home;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.rv193.spacepasschallenge.databinding.ItemPassRowBinding;
import com.example.rv193.spacepasschallenge.data.Response;
import java.util.ArrayList;
import java.util.List;

class SpacePassAdapter extends RecyclerView.Adapter<SpacePassBindingViewHolder> {

    @NonNull private List<Response> mItems = new ArrayList<>();

    SpacePassAdapter(@NonNull List<Response> items) {
        setItems(items);
    }

    @Override
    public SpacePassBindingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final ItemPassRowBinding binding = ItemPassRowBinding.inflate(LayoutInflater.from(parent.getContext()));
        return new SpacePassBindingViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(SpacePassBindingViewHolder holder, int position) {
        final Response response = mItems.get(position);
        holder.bind(response);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    void setItems(@NonNull List<Response> items) {
        mItems = items;
        notifyDataSetChanged();
    }
}
