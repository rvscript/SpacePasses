package com.example.rv193.spacepasschallenge.ui.home;

import android.support.annotation.NonNull;

import com.example.rv193.spacepasschallenge.ui.base.BaseContract;
import com.example.rv193.spacepasschallenge.data.Response;
import java.util.List;


public interface HomeContract {
    interface View extends BaseContract.View {
        void askForPermissions();
        void getLocation();
        void showResponses(@NonNull List<Response> responses);
        void showResponse(@NonNull Response response);
    }

    interface Presenter extends BaseContract.Presenter<View> {
        void start();
        void loadLocation();
        void loadResponses(double lat, double lng);
        void openResponse(@NonNull Response response);
    }
}