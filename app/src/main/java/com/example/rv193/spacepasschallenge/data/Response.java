package com.example.rv193.spacepasschallenge.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Response {

    @SerializedName("risetime")
    @Expose
    private Date risetime;
    @SerializedName("duration")
    @Expose
    private Double duration;

    public Date getRisetime() {
        return risetime;
    }

    public void setRisetime(Date risetime) {
        this.risetime = risetime;
    }

    public Double getDuration() {
        return duration;
    }

    public void setDuration(Double duration) {
        this.duration = duration;
    }

}