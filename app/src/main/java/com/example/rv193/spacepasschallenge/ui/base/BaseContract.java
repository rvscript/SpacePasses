package com.example.rv193.spacepasschallenge.ui.base;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

public interface BaseContract {

    interface View {
        void showMessage(@NonNull String string);
        void showMessage(@StringRes int message);
        void showLoading(boolean isLoading);
        void finishView();
    }

    interface Presenter<T extends View> {
        @SuppressWarnings("unchecked")
        void onViewAttached(@NonNull T view);
        void onViewDetached();
    }
}
