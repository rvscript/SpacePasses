package com.example.rv193.spacepasschallenge.di.component;

import android.app.Application;

import com.example.rv193.spacepasschallenge.SpacePassApplication;
import com.example.rv193.spacepasschallenge.di.module.ApiModule;
import com.example.rv193.spacepasschallenge.di.module.AppModule;
import com.example.rv193.spacepasschallenge.di.module.HomeModule;
import com.example.rv193.spacepasschallenge.di.module.RetrofitModule;
import com.example.rv193.spacepasschallenge.di.module.SpaceApiModule;
import com.example.rv193.spacepasschallenge.di.module.SpaceDataModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        ApiModule.class,
        HomeModule.class,
        RetrofitModule.class,
        SpaceApiModule.class,
        SpaceDataModule.class
})
public interface AppComponent {

    void inject(SpacePassApplication spacePassApplication);

    @Component.Builder
    interface Builder {

        AppComponent build();

        @BindsInstance
        Builder application(Application application);

        // This allows us to use another module in case we need mock responses, which there is no time to do
        Builder retrofitModule(RetrofitModule retrofitModule);
    }
}