package com.example.rv193.spacepasschallenge.ui.home;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.example.rv193.spacepasschallenge.R;
import com.example.rv193.spacepasschallenge.data.space.SpaceRepository;
import com.example.rv193.spacepasschallenge.di.ActivityScope;
import com.example.rv193.spacepasschallenge.scheduler.BaseSchedulerProvider;
import com.example.rv193.spacepasschallenge.ui.base.BasePresenter;
import com.example.rv193.spacepasschallenge.data.Response;
import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import timber.log.Timber;

@ActivityScope
public class HomePresenter extends BasePresenter<HomeContract.View> implements HomeContract.Presenter {

    @NonNull
    private final SpaceRepository mRepository;
    @NonNull private final BaseSchedulerProvider mScheduler;
    @VisibleForTesting
    boolean mHasLocationPermission = false;

    @Inject
    public HomePresenter(@NonNull SpaceRepository spaceRepository,
                         @NonNull BaseSchedulerProvider schedulerProvider) {
        mRepository = spaceRepository;
        mScheduler = schedulerProvider;
    }

    @Override
    public void start() {
        if (!mHasLocationPermission) {
            getView().askForPermissions();
        } else {
            loadLocation();
        }
    }

    @Override
    public void loadLocation() {
        getView().getLocation();
    }

    @Override
    public void loadResponses(double lat, double lng) {
        getView().showLoading(true);
        final Disposable disposable =
                mRepository.getResponses(lat, lng)
                        .subscribeOn(mScheduler.getIoThread())
                        .observeOn(mScheduler.getMainThread())
                        .subscribe(
                                responses -> {
                                    getView().showLoading(false);
                                    getView().showResponses(responses);
                                },
                                error -> {
                                    getView().showLoading(false);
                                    getView().showMessage(R.string.error_loading_responses);
                                    Timber.e(error);
                                }
                        );
        mCompositeDisposable.add(disposable);
    }

    @Override
    public void openResponse(@NonNull Response response) {
        getView().showResponse(response);
    }
}
