package com.example.rv193.spacepasschallenge.data.space;

import android.support.annotation.NonNull;

import com.example.rv193.spacepasschallenge.data.SpaceResponse;

import io.reactivex.Observable;

public interface SpaceDataSource {

    @NonNull
    Observable<SpaceResponse> getResponses(double lat, double lng);
}